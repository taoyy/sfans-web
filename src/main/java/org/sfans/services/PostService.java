package org.sfans.services;

import java.util.Date;

import org.sfans.domain.Post;
import org.springframework.data.domain.Page;

public interface PostService
{
    Page<Post> findAll(int pageNum);

    Page<Post> findByDateRange(Date start, Date end, int pageNum);

    Post findBySlug(String slug);
}
